package net.cat;

public class Cats {
    private String catName; // (кличка)
    private char gender; // (пол)
    private String dateOfBirth; // (дата рождения)
    private String catBreed; // (порода кошки)

    /**
     * Создание конструктора
     * @param catName
     * @param gender
     * @param dateOfBirth
     * @param catBreed
     */

    public Cats(String catName, char gender, String dateOfBirth, String catBreed) {
        this.catName = catName;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.catBreed = catBreed;
    }

    public String toString() {
        return "Привет! Моё имя " + catName + ". Я " + gender + " пола" + ". Моя порода " + catBreed + " Дата рождения: " + dateOfBirth ;
    }

    /**
     *Метод реализует диалог кота с пользователем
     */

    public void toTalk() {
        System.out.println("");
    }

    /**
     *Метод реализует диалог кота с пользователем
     */

    public void toWalk() {
        System.out.println("Мяу! Хочу гулять!");
    }

    /**
     *Метод реализует диалог кота с пользователем
     */

    public void  toEat() {
        System.out.println("Мяу! Хочу есть!");
    }
}