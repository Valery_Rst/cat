package net.cat;

public class CatsMain {
    public static void main(String[] args) {
        Cats[] cats = new  Cats[] {
                new Cats("Тишка ", 'М', "27.03.2010", "Абиссинская кот"),
                new Cats("Маша ", 'Ж', "27.06.2014", "Сиамская кошка"),
                new Cats("Жора ", 'М', "30.11.2017", "Анатолийский кот"),
                new Cats("Лёва ", 'Ж', "16.04.2011", "Бенгальская кошка")};

        for (int i = 0; i < cats.length; i++) {
            System.out.println(cats[i]);
        }
    }
}
